package com.nc.labs.agreements.digitaltv;
/**
 * Class which can be modify in the long run
 */
public class Channel {
    String name;

    public Channel(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
